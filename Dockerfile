FROM alpine:3.15

RUN apk add yarn jq nodejs --no-cache 

RUN yarn global add eslint && ln -sf /node_modules/eslint/bin/eslint.js /usr/bin/eslint
